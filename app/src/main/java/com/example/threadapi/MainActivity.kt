package com.example.threadapi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.widget.Button
import android.widget.Spinner
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import okio.IOException
import org.json.JSONObject
import org.mindrot.jbcrypt.BCrypt
import java.util.*

class MainActivity : AppCompatActivity() {
    var numberOfReq=0
    var ispost:Boolean =false
    val rigester_url = "http://59.120.189.128:5000/data/biologueData"
    var sum=0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        for(i in 0..20){
            Thread{
                val test_email=getRandomString(40)
                val test_username=getRandomString(40)
                val test_password=getRandomString(40)
                var jsonObject= JSONObject()
                var user_id=""
                jsonObject.put("post_t", 0)/////string
                jsonObject.put("username", test_username)/////string
                jsonObject.put("email", test_email)
                jsonObject.put("password", generateHashedPass(test_password))
                jsonObject.put("birthyear", 1996)
                jsonObject.put("license", 0)
                jsonObject.put("drink", 0)
                jsonObject.put("disease", 0)
                jsonObject.put("timepstamp", Date().time)
                var client = OkHttpClient()
                var mediaType = "application/json".toMediaType()
                var body = jsonObject.toString().toRequestBody(mediaType)
                var request: Request = Request.Builder()
                        .url(rigester_url)
                        .post(body)
                        .build()
                numberOfReq++;
                client.newCall(request).enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        println("fail : $e")
                        Log.e("Error", "$e")
                    }

                    override fun onResponse(call: Call, response: Response) {
                        numberOfReq--
                        ispost = true
                        user_id = response.body?.string().toString()
                    }
                })
                SystemClock.sleep(500)
                ////car
                var car_id=""
                var fetchObject= JSONObject()
                fetchObject.put("post_t", 1)/////string
                fetchObject.put("car_plate",getRandomString(7))/////string
                fetchObject.put("vehicle_t", 0)
                fetchObject.put("energy_t", 0)
                fetchObject.put("brand_t", 0)
                fetchObject.put("model_t", 0)
                fetchObject.put("tonnage",0)
                fetchObject.put("subjectID",user_id)
                fetchObject.put("timestamp", Date().time)
                client = OkHttpClient()
                mediaType = "application/json".toMediaType()
                body = fetchObject.toString().toRequestBody(mediaType)
                request= Request.Builder()
                    .url(rigester_url)
                    .post(body)
                    .build()
                numberOfReq++;
                client.newCall(request).enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        println("fail : $e")
                        Log.e("Error", "$e")
                    }

                    override fun onResponse(call: Call, response: Response) {
                        numberOfReq--
                        ispost = true
                        car_id= response.body?.string().toString()
                    }
                })
                SystemClock.sleep(5000)
                Log.e("$i","carID:$car_id,userID:$user_id")
                ///Algo
                if(!car_id.isNullOrEmpty() && !user_id.isNullOrEmpty()){
                    // Define the ECG and BCG sampling rate
                    val ECG_FS = 128
                    val BCG_FS = 64
                    val ECG_PERIOD = 1.0/ECG_FS*1000
                    val BCG_PERIOD = 1.0/BCG_FS*1000
                    // Unit: seconds
                    val Algo_SEND_TIME = 3
                    val ECG_SEND_TIME = 60
                    val BCG_SEND_TIME = 60
                    //sendData
                    lateinit var queue:MutableList<JSONObject?>
                    //ecg
                    lateinit var ECG_Algo_ID:MutableList<Any?>
                    lateinit var ecg:MutableList<Any?>
                    //bcg
                    lateinit var BCG_Algo_ID:MutableList<Any?>
                    lateinit var bcg:MutableList<Any?>
                    lateinit var acc:MutableList<Any?>
                    queue= arrayListOf()
                    BCG_Algo_ID= arrayListOf()
                    ECG_Algo_ID= arrayListOf()
                    bcg= arrayListOf()
                    acc= arrayListOf()
                    ecg= arrayListOf()
                    var Thread_BCG=Thread{
                        var data_len = 0
                        var bcg_cnt=0
                        while(true){
                            bcg.add((10..50000).random())
                            acc.add((0..360).random())
                            data_len++
                            bcg_cnt++
                            if(bcg_cnt%(BCG_FS*BCG_SEND_TIME)==0){
                                var getObject=JSONObject()
                                getObject.put("post_t", 3)/////string
                                getObject.put("bcg",bcg)
                                getObject.put("acc",acc)
                                getObject.put("device_t",1)
                                getObject.put("ECGID", JSONObject.NULL)
                                getObject.put("data_len",data_len)
                                getObject.put("id",BCG_Algo_ID)
                                getObject.put("id_len",2)
                                getObject.put("timestampST",0)
                                getObject.put("timestampEND",0)
                                getObject.put("timestamp",Date().time)
                                Log.e("BCG_Data_len","$data_len")
                                queue.add(getObject)
                                bcg.clear()
                                acc.clear()
                                BCG_Algo_ID.clear()
                                data_len=0
                            }
                            else if(bcg_cnt%(BCG_FS*Algo_SEND_TIME)==0){
                                var getObject=JSONObject()
                                getObject.put("post_t", 2)/////string
                                getObject.put("hr",(50..80).random())
                                getObject.put("sensor_t",1)
                                getObject.put("confidence",1)
                                getObject.put("resp",(10..20).random())
                                getObject.put("fatigue",(0..5).random())
                                getObject.put("status",(0..3).random())
                                getObject.put("subjectID",user_id)
                                getObject.put("carID",car_id)
                                getObject.put("timestamp",Date().time)
                                getObject.put("timestampM",0)
                                queue.add(getObject)
                            }
                            SystemClock.sleep(BCG_PERIOD.toLong())
                        }
                    }
                    var Thread_ECG=Thread{
                        var data_len=0
                        var ecg_cnt=0
                        while(true){
                            ecg.add((0..2048).random())
                            data_len+=1
                            ecg_cnt++
                            if (ecg_cnt%(ECG_FS*ECG_SEND_TIME)==0){
                                var getObject=JSONObject()
                                getObject.put("post_t", 4)/////string
                                getObject.put("ecg",ecg)
                                getObject.put("data_len",data_len)
                                getObject.put("id",ECG_Algo_ID)
                                getObject.put("id_len",2)
                                getObject.put("timestampST",1)
                                getObject.put("timestampEND",5)
                                getObject.put("timestamp",Date().time)
                                queue.add(getObject)
                                Log.e("ECG","Data_len:$data_len")
                                ecg.clear()
                                data_len=0
                                ECG_Algo_ID.clear()
                            }
                            SystemClock.sleep(ECG_PERIOD.toLong())
                        }
                    }
                    var Thread_sendDate=Thread{
                        var sendDate_counter=0
                        var Type2_cnt = 0
                        var Type3_cnt=0
                        var Type4_cnt=0
                        while (true){
                            if (!queue.isNullOrEmpty()){
                                sendDate_counter++
                                val temp = queue.get(0)
                                queue.removeAt(0)
                                var resStr=""
                                val client = OkHttpClient()
                                val mediaType = "application/json".toMediaType()
                                val body = temp.toString().toRequestBody(mediaType)
                                val request: Request = Request.Builder()
                                        .url(rigester_url)
                                        .post(body)
                                        .build()
                                numberOfReq++;
                                client.newCall(request).enqueue(object : Callback {
                                    override fun onFailure(call: Call, e: IOException) {
                                        println("fail : $e")
                                        Log.e("Error","connect failed.")
                                    }
                                    override fun onResponse(call: Call, response: Response) {
                                        numberOfReq--
                                        ispost = true
                                        resStr = response.body?.string().toString()
                                        if(temp?.getString("post_t")?.toInt()==2){
                                            Type2_cnt++
                                            BCG_Algo_ID.add(resStr)
                                            ECG_Algo_ID.add(resStr)
                                            Log.e("Thread:$i","$sendDate_counter")
                                            //Log.e("$sendDate_counter,Algo_counter:$Type2_cnt","RespondString:${BCG_Algo_ID}")
                                        }
                                        else if(temp?.getString("post_t")?.toInt()==3){
                                            Type3_cnt++
                                            //Log.e("$sendDate_counter,BCG_counter:$Type3_cnt", "RespondString:${resStr}")
                                        }
                                        else if(temp?.getString("post_t")?.toInt()==4){
                                            Type4_cnt++
                                            //Log.e("$sendDate_counter,ECG_counter:$Type4_cnt", "RespondString:${resStr}")
                                        }
                                    }
                                })
                            }
                            SystemClock.sleep(1000)
                        }
                    }
                    Thread_ECG.start()
                    Thread_BCG.start()
                    SystemClock.sleep(1000)
                    Thread_sendDate.start()
                }
            }.start()
        }
    }

    private fun generateHashedPass(pass: String): String {
        // hash a plaintext password using the typical log rounds (10)
        return BCrypt.hashpw(pass, BCrypt.gensalt())
    }
    private fun getRandomString(length: Int) : String {
        val charset = ('a'..'z') + ('A'..'Z') + ('0'..'9')
        return List(length) { charset.random() }
                .joinToString("")
    }

}